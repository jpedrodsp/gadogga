import sys
import random
import file
import numbers, time
from random import randint
import generate

average_exec_time = 3

def parent(i):
    return (i//2)

def left(i):
    return (2*i+1)

def right(i):
    return (2*i + 2)

def max_heapify(a, i, heap_size, ic = 0):
    l = left(i)
    r = right(i)
    ic += 3
    if l <= heap_size and a[l] > a[i]:
        largest = l
    else:
        largest = i
    ic += 3
    if r <= heap_size and a[r] > a[largest]:
        largest = r
        
    ic += 1
    if i != largest:
        a[i], a[largest] = a[largest], a[i]
        ic += max_heapify(a, largest, heap_size)
    return ic

def build_max_heap(a, heap_size, ic = 0):
    for i in range(heap_size//2 - 1, -1, -1):
        ic += 1
        #print(i)
        ic += max_heapify(a, i, heap_size) 
    return ic

def heap_sort(a, heap_size, ic = 0):
    ic += build_max_heap(a, heap_size)
    for i in range(len(a)-1,0 , -1):
        ic += 1
        a[0], a[i] = a[i], a[0]
        heap_size = heap_size - 1
        ic += max_heapify(a, 0, heap_size)
    return ic

def her_exec(list):
    heap_size = len(list[1:])
    return heap_sort(list, heap_size)

def func(list, list_num, name, result, debug=True, random=False):
    average_time_list = []
    average_comp_list = []
    for i in range(0, average_exec_time):
            initial_time = time.time()
            # Algoritmo
            if list is [] or random is True:
                list = generate.gen_random(list_num)
                initial_time = time.time()
                ic = her_exec(list)
            else:
                ic = her_exec(list)
            final_time = time.time()
            if debug:
                print("Heapsort (" + str(list_num) + ") [" + str(i+1) + "]: ")
                print("Initial time: " + "%.22f s" % initial_time)
                print("Final time: " + "%.22f s" % final_time)
                print("Difference: " + "%.22f s" % (final_time - initial_time) )
                print("")
            average_time_list.append((final_time - initial_time))
            average_comp_list.append(ic)
            result["time_heapsort_" + name] = "%.22f s" % numbers.media(average_time_list)
            result["comp_heapsort_" + name] = "%.2f comp" % numbers.media(average_comp_list)
    pass

def run(debug=False):
    lists = [100, 500, 1000, 5000, 30000, 80000, 100000, 150000, 200000]

    for list_num in lists:
        #Clean result list
        result = {}

        # Execute crescent order
        list = file.read_list("list_oc_" + str(list_num))
        func(list, list_num, "list_oc_" + str(list_num), result)

        # Execute decrescent order
        list = file.read_list("list_od_" + str(list_num))
        func(list, list_num, "list_od_" + str(list_num), result)
        
        # Execute random order
        list = file.read_list("list_r_" + str(list_num))
        func([], list_num, "list_r_" + str(list_num), result, random=True)

        file.result_append(result, "heapsort")

    # End
    #file.result_write(result, "heapsort")
    pass
