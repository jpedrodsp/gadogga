import sys
import random
import file
import numbers, time
import generate

average_exec_time = 3

def insertionsort(A):
    ic = 0
    for j in range(0,len(A)):
        ic += 1
        key =  A[j]       
        k = j - 1
        while k >= 0 and A[k] > key:
            ic += 3
            A[k + 1] = A[k]
            k -= 1
        A[k + 1] = key
    return ic
      

def func(list, list_num, name, result, debug=True, random=False):
    average_time_list = []
    average_comp_list = []
    for i in range(0, average_exec_time):
            initial_time = time.time()
            # Algoritmo
            if list is [] or random is True:
                list = generate.gen_random(list_num)
                initial_time = time.time()
                ic = insertionsort(list)
            else:
                ic = insertionsort(list)
            final_time = time.time()
            if debug:
                print("Insertion Sort (" + str(list_num) + ") [" + str(i+1) + "]: ")
                print("Initial time: " + "%.22f s" % initial_time)
                print("Final time: " + "%.22f s" % final_time)
                print("Difference: " + "%.22f s" % (final_time - initial_time) )
                print("")
            average_time_list.append((final_time - initial_time))
            average_comp_list.append(ic)
            result["time_insertionsort_" + name] = "%.22f s" % numbers.media(average_time_list)
            result["comp_insertionsort_" + name] = "%.2f comp" % numbers.media(average_comp_list)
    pass

def run(debug=False):
    lists = [100, 500, 1000, 5000, 30000, 80000, 100000, 150000, 200000]

    for list_num in lists:
        #Clean result list
        result = {}

        # Execute crescent order
        list = file.read_list("list_oc_" + str(list_num))
        func(list, list_num, "list_oc_" + str(list_num), result)

        # Execute decrescent order
        list = file.read_list("list_od_" + str(list_num))
        func(list, list_num, "list_od_" + str(list_num), result)
        
        # Execute random order
        list = file.read_list("list_r_" + str(list_num))
        func([], list_num, "list_r_" + str(list_num), result, random=True)

        file.result_append(result, "insertionsort")

    # End
    #file.result_write(result, "insertionsort")
    pass
