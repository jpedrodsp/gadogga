import sys
import random
import file
import numbers, time
import generate

average_exec_time = 3

def merge(array, p, q, r, ic=0):
    # print("\t\tin merge, ", array, p,q,r)
    # Calculate size
    left_size = q - p + 1
    right_size = r - q
    # print("\t\t", "sizes: ", left_size, right_size, end=' ... ')

    # Create array and fill them
    left_array = []
    right_array = []
    left_array = array[p:q+1]
    ic += left_size
    right_array = array[q+1:r+1]
    ic += right_size
    # print(left_array, right_array)

    i, j, k = 0, 0, p

    while i < left_size and j < right_size:
        ic += 1
        ic += 1
        if left_array[i] <= right_array[j]:
            ic += 2
            # print("switching array's", array[k], "with left array", left_array[i])
            array[k] = left_array[i]
            i += 1
            k += 1
        else:
            ic += 1
            # print("switching array's", array[k], "with right array", right_array[j])
            array[k] = right_array[j]
            j += 1
            k += 1
    # When one of the arrays end, fill the rest (they will be ordered, anyway)
    while i < left_size:
        ic += 1
        # print("\t\tfilling remaining with left array", left_array[i])
        array[k] = left_array[i]
        i = i + 1
        k = k + 1
    while j < right_size:
        ic += 1
        # print("\t\tfilling remaining with right array", right_array[j])
        array[k] = right_array[j]
        j = j + 1
        k = k + 1
    return ic

def mergesort(array, p, r, ic = 0):
    ic += 1
    if p < r:
        q = (p + r) // 2
        # print("calling l mergesort", p, q)
        ic += mergesort(array, p, q)
        # print("calling r mergesort", q+1, r)
        ic += mergesort(array, q + 1, r)
        # print("\tentering merge: ", array, p, q, r)
        ic += merge(array, p, q, r)
        # print("\tdidnt call, p is not lesser than r")
    return ic

def func(list, list_num, name, result, debug=True, random=False):
    average_time_list = []
    average_comp_list = []
    for i in range(0, average_exec_time):
            initial_time = time.time()
            # Algoritmo
            if list is [] or random is True:
                list = generate.gen_random(list_num)
                initial_time = time.time()
                ic = mergesort(list, 0, len(list)-1)
            else:
                ic = mergesort(list, 0, len(list)-1)
            final_time = time.time()
            if debug:
                print("Mergesort (" + str(list_num) + ") [" + str(i+1) + "]: ")
                print("Initial time: " + "%.22f s" % initial_time)
                print("Final time: " + "%.22f s" % final_time)
                print("Difference: " + "%.22f s" % (final_time - initial_time) )
                print("")
            average_time_list.append((final_time - initial_time))
            average_comp_list.append(ic)
            result["time_mergesort_" + name] = "%.22f s" % numbers.media(average_time_list)
            result["comp_mergesort_" + name] = "%.2f comp" % numbers.media(average_comp_list)
    pass

def run(debug=False):
    lists = [100, 500, 1000, 5000, 30000, 80000, 100000, 150000, 200000]

    for list_num in lists:
        #Clean result list
        result = {}

        # Execute crescent order
        list = file.read_list("list_oc_" + str(list_num))
        func(list, list_num, "list_oc_" + str(list_num), result)

        # Execute decrescent order
        list = file.read_list("list_od_" + str(list_num))
        func(list, list_num, "list_od_" + str(list_num), result)
        
        # Execute random order
        list = file.read_list("list_r_" + str(list_num))
        func([], list_num, "list_r_" + str(list_num), result, random=True)

        file.result_append(result, "mergesort")

    # End
    #file.result_write(result, "mergesort")
    pass
