
def start():
    list = []
    print("ordered d", test_ordered_crescent(list))
    print("ordered c", test_ordered_decrescent(list))
    pass

def test_ordered_crescent(list, debug=False):
    if debug:
        for i in range(0, len(list)):
            print(list[i], end=' ')
        print("")
    for i in range(0, len(list)):
        if i == 0:
            pass
        else:
            if list[i] < list[i-1]:
                print("i:", i, "i-1:",i-1, list[i], list[i-1])
                print("ordered (crescent): ", len(list), "elem failed")
                return False
    return True

def test_ordered_decrescent(list):
    for i in range(0, len(list)):
        if i == 0:
            pass
        else:
            if list[i] > list[i-1]:
                print(i,i-1, list[i], list[i-1])
                print("ordered (decrescent): ", len(list), "failed")
                return False
    return True