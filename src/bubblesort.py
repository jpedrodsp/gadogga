import file
import numbers
import time
import generate

average_exec_time = 3

def bubblesort(a):
    ic = 0
    for i in range(len(a)-1,0,-1):
        ic += 1
        for j in range(i):
            ic += 1
            if a[j]>a[j+1]:
                aux = a[j]
                a[j] = a[j+1]
                a[j+1] = aux
            ic += 1
    return ic

def func(list, list_num, name, result, debug=True, random=False):
    average_time_list = []
    average_comp_list = []
    for i in range(0, average_exec_time):
            initial_time = time.time()
            # Algoritmo
            if list is [] or random is True:
                initial_time = time.time()
                list = generate.gen_random(list_num)
                ic = bubblesort(list)
            else:
                ic = bubblesort(list)
            final_time = time.time()
            if debug:
                print("Bubblesort (" + str(list_num) + ") [" + str(i+1) + "]: ")
                print("Initial time: " + "%.22f s" % initial_time)
                print("Final time: " + "%.22f s" % final_time)
                print("Difference: " + "%.22f s" % (final_time - initial_time) )
                print("")
            average_time_list.append((final_time - initial_time))
            average_comp_list.append(ic)
            result["time_bubblesort_" + name] = "%.22f s" % numbers.media(average_time_list)
            result["comp_bubblesort_" + name] = "%.2f comp" % numbers.media(average_comp_list)
    pass

def run(debug=False):
    lists = [100, 500, 1000, 5000, 30000, 80000, 100000, 150000, 200000]

    for list_num in lists:
        #Clean result list
        result = {}

        # Execute crescent order
        list = file.read_list("list_oc_" + str(list_num))
        func(list, list_num, "list_oc_" + str(list_num), result)

        # Execute decrescent order
        list = file.read_list("list_od_" + str(list_num))
        func(list, list_num, "list_od_" + str(list_num), result)
        
        # Execute random order
        list = file.read_list("list_r_" + str(list_num))
        func([], list_num, "list_r_" + str(list_num), result, random=True)

        file.result_append(result, "bubblesort")

    # End
    #file.result_write(result, "bubblesort")
    pass
