# Import Sort Algorithms
import bubblesort
import mergesort
import quicksort
import heapsort
import insertionsort
import hybrid_quickinsertion_gadosort

# Import info
import computer_info

# Print computer info
try:
    computer_info.info()
except:
    print("Failed to get computer info")
    f = open("computerinfo.fail", "w+")
    f.close()

'''
 # Print data info
try:
    bubblesort.run()
except:
    print("Failed at bubblesort")
    f = open("bubblesort.fail", "w+")
    f.close()
'''

try:
    mergesort.run()
except:
    print("Failed at mergesort")
    f = open("mergesort.fail", "w+")
    f.close()

try:
    quicksort.run()
except:
    print("Failed at quicksort")
    f = open("quicksort.fail", "w+")
    f.close()

try:
    heapsort.run()
except:
    print("Failed at heapsort")
    f = open("heapsort.fail", "w+")
    f.close()

try:
    insertionsort.run()
except:
    print("Failed at insertionsort")
    f = open("insertionsort.fail", "w+")
    f.close()

try:
    hybrid_quickinsertion_gadosort.run()
except:
    print("Failed at gadosort")
    f = open("gadosort.fail", "w+")
    f.close()
