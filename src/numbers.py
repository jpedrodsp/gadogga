import file
import random

def gen_random(numbers):
    list = []
    for i in range(0, numbers):
        list.append(int(random.random() * 10000))
    return list

'''
list100 = gen_random(100)
list500 = gen_random(500)
list1000 = gen_random(1000)
list5000 = gen_random(5000)
list30000 = gen_random(30000)
list80000 = gen_random(80000)
list100000 = gen_random(100000)
list150000 = gen_random(150000)
list200000 = gen_random(200000)

file.write_list(list100, "list100")
file.write_list(list500, "list500")
file.write_list(list1000, "list1000")
file.write_list(list5000, "list5000")
file.write_list(list30000, "list30000")
file.write_list(list80000, "list80000")
file.write_list(list100000, "list100000")
file.write_list(list150000, "list150000")
file.write_list(list200000, "list200000")
'''

def media(values):
    final = float(0)
    for i in values:
        final += i
    return final / len(values)