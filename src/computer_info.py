import sys
import os
import platform
import cpuinfo

def info():
    system_data = platform.uname()
    print(system_data[1] + ", " + system_data[0] + " " + system_data[2] + " " + system_data[3] + " " + system_data[4])
    print("Processor: " + cpuinfo.cpu.info[0]['model name'] + " x" + cpuinfo.cpu.info[0]['cpu cores'] + " " + cpuinfo.cpu.info[0]['cpu MHz'] + "MHz")
    print("Compiler info: " + "Python " +  sys.version)
    print("RAM Info: undefined")