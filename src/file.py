import computer_info
import datetime

def write_list(list, name):
    f = open("lists/" + (str(name) + ".dat"), "w+")
    for i in list:
        f.write(str(i) + '\n')
    f.close()  
    pass

def read_list(name):
    list = []
    f = open("lists/" + (str(name) + ".dat"), "r")
    a = f.readline()
    while (a != ""):
        list.append(int(a))
        a = f.readline()
    f.close()  
    return list

def result_write(result, name):
    f = open("results/" + (str(name) + ".txt"), "w+")
    f.write("[" + str(datetime.datetime.now()) + "]\n")
    for i in result:
        print(i, result.get(i))
        f.write(str(i) + " = " + str(result.get(i)) + '\n')
    f.close()
    pass

def result_append(result, name):
    f = open("results/" + (str(name) + ".txt"), "a+")
    f.write("[" + str(datetime.datetime.now()) + "]\n")
    for i in result:
        print(i, result.get(i))
        f.write(str(i) + " = " + str(result.get(i)) + '\n')
    f.close()
    pass

def report():
    f = open("results\\" + "report.txt", "w+")
    f.write(str(computer_info.info()))
    f.close()