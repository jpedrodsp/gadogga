import os
import random
import file

max_num_size = 500000
max_random_offset = 200

def gen_random(size):
    list = []
    for _ in range(0, size):
        list.append(int(random.random() * max_num_size))
    return list

def gen_ordered_crescent(size):
    list = []
    for _ in range(0, size):
        if len(list) == 0:
            list.append(random.randint(0, max_random_offset))
            continue
        list.append(int(list[len(list) - 1]) + random.randint(0, max_random_offset))
    return list

def gen_ordered_decrescent(size):
    list = []
    for _ in range(0, size):
        if len(list) == 0:
            list.append(random.randint(0, max_random_offset))
            continue
        list.append(int(list[len(list) - 1]) + random.randint(0, max_random_offset))
    return list[::-1]

def gen_lists():
    size = [5,100,500,1000,5000,30000,80000,100000,150000,200000]
    for i in size:
        file.write_list(gen_random(i), "list_r_" + str(i))
        file.write_list(gen_ordered_crescent(i), "list_oc_" + str(i))
        file.write_list(gen_ordered_decrescent(i), "list_od_" + str(i))

# gen_lists()