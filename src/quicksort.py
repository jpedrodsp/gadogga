import sys
import random
import file
import numbers, time
import generate

average_exec_time = 3
sys.setrecursionlimit(500000)

def Partition(A,p,r,ic=0):
    i = (p - 1)
    x = A[r]
    for j in range(p,r):
        ic += 3
        if A[j] <= x :
            i += 1
            A[j], A[i] = A[i], A[j]
    A[r], A[i + 1] = A[i + 1], x
    return i+1

def quicksort(A,p,r):
        ic = 1
        if(p < r):
                q = Partition(A,p,r,ic)
                ic += quicksort(A,p,q-1)
                ic += quicksort(A,q+1,r)
        return ic
    
def func(list, list_num, name, result, debug=True, random=False):
    average_time_list = []
    average_comp_list = []
    for i in range(0, average_exec_time):
            initial_time = time.time()
            # Algoritmo
            if list is [] or random is True:
                list = generate.gen_random(list_num)
                initial_time = time.time()
                ic = quicksort(list, 0, len(list)-1)
            else:
                ic = quicksort(list, 0, len(list)-1)
            final_time = time.time()
            if debug:
                print("Quicksort (" + str(list_num) + ") [" + str(i+1) + "]: ")
                print("Initial time: " + "%.22f s" % initial_time)
                print("Final time: " + "%.22f s" % final_time)
                print("Difference: " + "%.22f s" % (final_time - initial_time) )
                print("")
            average_time_list.append((final_time - initial_time))
            average_comp_list.append(ic)
            result["time_quicksort_" + name] = "%.22f s" % numbers.media(average_time_list)
            result["comp_quicksort_" + name] = "%.2f comp" % numbers.media(average_comp_list)
    pass

def run(debug=False):
    lists = [100, 500, 1000, 5000, 30000,80000, 100000, 150000, 200000]

    for list_num in lists:
        #Clean result list
        result = {}

        # Execute crescent order
        list = file.read_list("list_oc_" + str(list_num))
        func(list, list_num, "list_oc_" + str(list_num), result)

        # Execute decrescent order
        list = file.read_list("list_od_" + str(list_num))
        func(list, list_num, "list_od_" + str(list_num), result)
        
        # Execute random order
        list = file.read_list("list_r_" + str(list_num))
        func([], list_num, "list_r_" + str(list_num), result, random=True)

        file.result_append(result, "quicksort")

    # End
    #file.result_write(result, "quicksort")
    pass
