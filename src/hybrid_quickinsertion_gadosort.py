import file
import numbers
import time
import generate
import mytest

average_exec_time = 3

def insertionsort(A, ic):
    for j in range(0,len(A)):
        ic += 1
        key =  A[j]       
        k = j - 1
        while k >= 0 and A[k] > key:
            ic += 1
            A[k + 1] = A[k]
            k -= 1
        A[k + 1] = key
    ic += 1
    return ic

def partition_lomuto(arr, lo, hi, ic):
    pivot = arr[hi]
    i = lo
    for j in range(lo, hi):
        ic += 1
        ic += 1
        if arr[j] <= pivot:
            arr[i], arr[j] = arr[j], arr[i]
            i = i + 1
    arr[i], arr[hi] = arr[hi], arr[i]
    return ic, i-1

def quicksort(A,p,r, ic,listnum):
    ic += 1
    if (p < r):
        ic += 1
        if (r - p < listnum/10): # número definido com critérios baseados na tabela de tempos anterior
            ic += insertionsort(A, 0)
        else:
            tic, q = partition_lomuto(A,p,r,0)
            ic += tic
            ic += quicksort(A,p,q-1,0, listnum)
            ic += quicksort(A,q+1,r,0, listnum)
    return ic

def gadosort(list, ic, listnum): # é um algoritmo proposto por dois bois na frente de um teclado
    ic = quicksort(list, 0, len(list)-1, 0, listnum)
    return ic

def func(list, list_num, name, result, debug=True, random=False):
    average_time_list = []
    average_comp_list = []
    ic = 0
    for i in range(0, average_exec_time):
            initial_time = time.time()
            # Algoritmo
            if list is [] or random is True:
                initial_time = time.time()
                list = generate.gen_random(list_num)
                gadosort(list, ic, list_num)
            else:
                gadosort(list, ic, list_num)
            final_time = time.time()
            if debug:
                print("Gadosort (" + str(list_num) + ") [" + str(i+1) + "]: ")
                print("Initial time: " + "%.22f s" % initial_time)
                print("Final time: " + "%.22f s" % final_time)
                print("Difference: " + "%.22f s" % (final_time - initial_time) )
                print("")
            average_time_list.append((final_time - initial_time))
            average_comp_list.append(ic)
            result["time_gadosort_" + name] = "%.22f s" % numbers.media(average_time_list)
            result["comp_gadosort_" + name] = "%.2f comp" % numbers.media(average_comp_list)
    return list

def run(debug=False):
    lists = [100, 500, 1000, 5000, 30000, 80000, 100000, 150000, 200000]

    for list_num in lists:
        #Clean result list
        result = {}

        # Execute crescent order
        list = file.read_list("list_oc_" + str(list_num))
        list = func(list, list_num, "list_oc_" + str(list_num), result)

        # Execute decrescent order
        list = file.read_list("list_od_" + str(list_num))
        list = func(list, list_num, "list_od_" + str(list_num), result)
        
        # Execute random order
        list = file.read_list("list_r_" + str(list_num))
        list = func([], list_num, "list_r_" + str(list_num), result, random=True)
    
        file.result_append(result, "gadosort")

    # End
    #file.result_write(result, "gadosort")
    pass